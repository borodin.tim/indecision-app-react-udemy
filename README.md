Indecision App

Created within the Udemy React course

Create a list of tasks to do and let the computer choose what task you should do next.

Uses: React, webpack, babel, react modal, scss



TODOs:
1. Add tests with Jest
2. Change styling to lighter a bit
3. Add Firebase auth and database