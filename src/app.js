import React from 'react';
import ReactDOM from 'react-dom';
import IndecisionApp from './components/IndecisionApp';
import 'normalize.css/normalize.css';
import './styles/styles.scss';

ReactDOM.render(<IndecisionApp />, document.getElementById('app'));
// ReactDOM.render(<IndecisionApp options={['Option One', 'Option Two']} />, document.getElementById('app'));
// ReactDOM.render(<User name="Tim" age="32"/>, document.getElementById('app'));