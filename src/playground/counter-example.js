class Counter extends React.Component {
    constructor(props) {
        super(props);
        this.handlePlusOne = this.handlePlusOne.bind(this);
        this.handleMinusOne = this.handleMinusOne.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.state = {
            count: 0
        };
    }

    componentDidMount() {
        const json = localStorage.getItem('count');
        const count = JSON.parse(json);
        
        if (!isNaN(count)) {
            this.setState(() => {
                return { count };
            });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        try {
            if (prevState.count !== this.state.count) {
                localStorage.setItem('count', JSON.stringify(this.state.count));
            }
        } catch (error) {

        }
    }

    handlePlusOne() {
        this.setState((prevState) => {
            return {
                count: prevState.count + 1
            };
        });
    }

    handleMinusOne() {
        this.setState((prevState) => {
            return {
                count: prevState.count - 1
            };
        });
    }

    handleReset() {
        this.setState(() => {
            return {
                count: 0
            };
        });

        // old style
        // this.setState({
        //     count: 0
        // });
        // this.setState({
        //     count: this.state.count +1
        // });
    }

    render() {
        return (
            <div>
                <h1>Count: {this.state.count}</h1>
                <button onClick={this.handlePlusOne}>+1</button>
                <button onClick={this.handleMinusOne}>-1</button>
                <button onClick={this.handleReset}>Reset</button>
            </div>
        );
    }
}

ReactDOM.render(<Counter count={6} />, document.getElementById('app'));

// let count = 0;
// const addOne = () => {
//     count++;
//     renderCounterApp();
//     console.log('addOne', count);
// }
// const minusONe = () => {
//     count--;
//     renderCounterApp();
//     console.log('minusOne');
// }
// const reset = () => {
//     count = 0;
//     renderCounterApp();
//     console.log('reset');
// }
// const template2 = (
//     <div>
//         <h1>Count: {count}</h1>
//         <button onClick={addOne}>+1</button>
//         <button onClick={minusONe}>-1</button>
//         <button onClick={reset}>Reset</button>
//     </div>
// );

// const appRoot = document.getElementById('app');

// ReactDOM.render(template, appRoot);

// const renderCounterApp = () => {
//     const template2 = (
//         <div>
//             <h1>Count: {count}</h1>
//             <button onClick={addOne}>+1</button>
//             <button onClick={minusONe}>-1</button>
//             <button onClick={reset}>Reset</button>
//         </div>
//     );
//     ReactDOM.render(template2, appRoot);
// }