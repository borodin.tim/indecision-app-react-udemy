// Arguments object - no longer bound with arrow functions

const add = (a, b) => {
    // console.log('arguments: ', arguments); // don't exist in arrow funcs
    return a + b;
}
console.log(add(85, 1, 1001));




// this keywork - no longer bound
const user = {
    name: 'Tim',
    cities: ['Irkutsk', 'Holon', 'Moscow'],
    printPlacesLived() {
        return this.cities.map((city) => this.name + ' has lived in ' + city);

        // Problem when using ES5 function 
        // this.cities.forEach(function (city) {
        //     console.log(this.name + ' has lived in ' + city); // this.name is undefined
        // });

        // Solution 1
        // const that = this;
        // this.cities.forEach(function (city) {
        //     console.log(that.name + ' has lived in ' + city);
        // });

        // Solution 2 - 
        // this.cities.forEach((city) => {
        //     console.log(this.name + ' has lived in ' + city);
        // });
    }
};
console.log(user.printPlacesLived());



const multiplier = {
    numbers: [1, 2, 3],
    multiplyBy: 2,
    multiply() {
        return this.numbers.map((n) => n * this.multiplyBy)
    }
}
console.log(multiplier.multiply());