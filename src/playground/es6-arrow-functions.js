function square(x) {
    return x * x;
};

console.log('square: ', square(8));


// const squareArrow = (x) => {
//     return x * x;
// };

const squareArrow = (x) => x * x;

console.log('squareArrow: ', squareArrow(4));



const getFirstName = (x) => x.split(' ')[0];
console.log(getFirstName('Bob Smith'));
