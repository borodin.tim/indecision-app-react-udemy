const showDetails = 'Show Details';
const hideDetails = 'Hide Details';

class VisibilityToggle extends React.Component {
    constructor(props) {
        super(props);
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
        this.state = {
            visibility: false
        }
    }

    handleToggleVisibility() {
        this.setState((prevState) => {
            return {
                visibility: !prevState.visibility
            }
        });
    }

    render() {
        return (
            <div>
                <h1>{'Visibility toggle'}</h1>
                <button onClick={this.handleToggleVisibility}>{this.state.visibility ? hideDetails : showDetails}</button>
                {this.state.visibility && (
                    <div>
                        <p>This are the hidden details.</p>
                    </div>
                )}
            </div>
        );
    }
}

ReactDOM.render(<VisibilityToggle />, document.getElementById('app'));




// Old verison without using React Components

// let visibility = true;

// const toggleVisibility = () => {
//     visibility = !visibility;
//     if (visibility) {
//         buttonText = showDetails;
//     } else {
//         buttonText = hideDetails;
//     }
//     render();
// }

// const jsx = (
//     <div>
//         <h1>{'Visibility toggle'}</h1>
//         <button onClick={handleToggleVisibility}>{buttonText}</button>
//         <p hidden={visibility}>This are the hidden details.</p>
//     </div>
// );

// const appRoot = document.getElementById('app');

// ReactDOM.render(jsx, appRoot);

// const render = () => {
//     const jsx = (
//         <div>
//             <h1>{app.title}</h1>
//             <button onClick={handleToggleVisibility}>{buttonText}</button>
//             <p hidden={visibility}>This are the hidden details.</p>
//         </div>
//     );
//     ReactDOM.render(jsx, appRoot);
// }