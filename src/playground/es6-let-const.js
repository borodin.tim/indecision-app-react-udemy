var nameVar = 'Tim';
var nameVar = 'Mara';
console.log('nameVar: ', nameVar);

let nameLet = 'Harley';
nameLet = 'Delain';
console.log('nameLet: ', nameLet);

const nameConst = 'Bob';
console.log('Bob: ', nameConst);

function getPetName() {
    let petName = 'Hal';
    return petName;
}
const petName = getPetName();
console.log(petName)


// Bock scoping
const fullName = 'Tim Borodin';
let firstName;
if (fullName) {
    firstName = fullName.split(' ')[0];
    console.log('firstName: ', firstName);
}
console.log('firstName: ', firstName);
